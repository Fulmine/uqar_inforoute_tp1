/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.acos;
import static java.lang.Math.asin;
import static java.lang.Math.sqrt;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jimmyking
 */
@WebServlet(urlPatterns = {"/checkTriangle"})
public class checkTriangle extends HttpServlet {
        
    public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
}
    
    public triangle calculatePoints(double a, double b, double c)
    {
        triangle newTriangle = new triangle();
       
        double s = (a + b + c) / 2;
        double area = sqrt(s * (s - a) * (s - b) * (s - c));

        double R = (a * b * c) / (4 * area);
        
        double a2 = Math.pow(a, 2);
        double b2 = Math.pow(b, 2);
        double c2 = Math.pow(c, 2);

        double cosA = (b2 + c2 - a2) / (2 * b * c);
        double angleA = acos(cosA) * (180 / Math.PI);
        
        double cosB = (c2 + a2 - b2) / (2 * c * a);
        double angleB = acos(cosB) * (180 / Math.PI);   
        
        double angleC = 180 - angleA - angleB;
        
        newTriangle.getA().setAngle(angleA);
        newTriangle.getA().setLenght(a);
        
        newTriangle.getB().setAngle(angleB);
        newTriangle.getB().setLenght(b);
        
        newTriangle.getC().setAngle(angleC);
        newTriangle.getC().setLenght(c);
        
        
        newTriangle.setName(getTriangleName(newTriangle));
        return newTriangle;
    }
    
    public boolean isRight(triangle currentTriangle) {
        if (round(currentTriangle.getA().getAngle(), 1) == 90.0 || round(currentTriangle.getB().getAngle(), 1) == 90.0 || round(currentTriangle.getC().getAngle(), 1) == 90.0)
            return true;
        return false;
    }
    
    public boolean isObtus(double a2, double b2, double c2)
    {
        if ( a2 + b2 < c2 || c2 + a2 < b2 || c2 + b2 < a2)
            return true;
        return false;
    }
    
    public boolean isAcute(double a2, double b2, double c2)
    {
        if ( a2 + b2 > c2 || c2 + a2 > b2 || c2 + b2 > a2)
            return true;
        return false;
    }
    
    public String getTriangleName(triangle currentTriangle)
    {
        double a = currentTriangle.getA().getLenght();
        double b = currentTriangle.getB().getLenght();
        double c = currentTriangle.getC().getLenght();
        
        double a2 = Math.pow(a, 2);
        double b2 = Math.pow(b, 2);
        double c2 = Math.pow(c, 2);
        
        if (a == b && b == c)
        { 
            return "&Eacute;quilat&eacute;ral";
        }
        
        if (isRight(currentTriangle))
            return "Triangle rectangle";
        
        if (a == b || a == c || b == c) {
            if (isObtus(a2, b2, c2))
                return "Isoc&egrave;le obtus";
            if (isAcute(a2, b2, c2))
                return "Isoc&egrave;le aigu";
            return "Isoc&egrave;le";
        } 
        if (isObtus(a2, b2, c2))
            return "Scal&egrave;ne obtus";
        if (isAcute(a2, b2, c2))
            return "Scal&egrave;ne aigu";
        return "Scal&egrave;ne";
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        double a = Double.parseDouble(request.getParameter("c1"));
        double b = Double.parseDouble(request.getParameter("c2"));
        double c = Double.parseDouble(request.getParameter("c3"));
        
        triangle calculatedTriangle = calculatePoints(a, b, c);
                
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html lang=\"en\">");
            out.println("  <head>");
            out.println("    <meta charset=\"utf-8\">");
            out.println("    <title>Calculateur de Triangles</title>");
            out.println("    <link href=\"assets/css/bootstrap.min.css\" rel=\"stylesheet\">");
            out.println("    <link href=\"assets/css/custom.css\" rel=\"stylesheet\">");
            out.println("  </head>");
            out.println("  <body>");
            out.println("    <div class=\"site-wrapper\">");
            out.println("      <div class=\"site-wrapper-inner\">");
            out.println("        <div class=\"cover-container\">");
            out.println("          <div class=\"header clearfix\">");
            out.println("            <div class=\"inner\">");
            out.println("              <nav class=\"nav nav-header\">");
            out.println("                <a class=\"nav-link active\" href=\"index.html\">Accueil</a>");
            out.println("                <a class=\"nav-link\" href=\"apropos.jsp\">&Agrave; propos</a>");
            out.println("              </nav>");
            out.println("            </div>");
            out.println("          </div>");
            out.println("");
            out.println("          <div class=\"inner cover\">");
            out.println("            <img alt=\"logo\" src=\"assets/img/logo.png\"/>");
            out.println("            <h1 class=\"cover-heading\">Calculateur de Triangle</h1>");
            out.println("            <p>Le triangle est " + calculatedTriangle.getName() + "</p>");
            out.println("            <table>");
            out.println("			<tr>");
            out.println("                           <th>Point</th>");
            out.println("                           <th>Longeur</th>");
            out.println("                           <th>Angle</th>");
            out.println("			</tr>");
            out.println("			<tr>");
            out.println("                           <td>A</td>");
            out.println("                           <td>" + calculatedTriangle.getA().getLenght() +"</td>");
            out.println("                           <td>" + String.format( "%.1f&deg;", calculatedTriangle.getA().getAngle()) + "</td>");
            out.println("			</tr>");
            out.println("			<tr>");
            out.println("                           <td>B</td>");
            out.println("                           <td>" + calculatedTriangle.getB().getLenght() + "</td>");
            out.println("                           <td>" + String.format( "%.1f&deg;", calculatedTriangle.getB().getAngle()) + "</td>");
            out.println("			</tr>");
            out.println("			<tr>");
            out.println("                           <td>C</td>");
            out.println("                           <td>" + calculatedTriangle.getC().getLenght() + "</td>");
            out.println("                           <td>" + String.format( "%.1f&deg;", calculatedTriangle.getC().getAngle()) + "</td>");
            out.println("			</tr>");
            out.println("		</table>");
            out.println("		<center><img src=\"assets/img/"+ calculatedTriangle.getImagePath() +"\" alt=\"triangle\"/></center>");
            out.println("            <a href=\"index.html\" class=\"btn btn-primary\">Retour</a>");
            out.println("          </div>");
            out.println("        </div>");
            out.println("      </div>");
            out.println("    </div>");
            out.println("    <script src=\"assets/js/jquery-3.2.1.min.js\" />");
            out.println("    <script src=\"assets/js/bootstrap.min.js\"></script>");
            out.println("  </body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
