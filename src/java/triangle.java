/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jimmyking
 */
public class triangle {
        
    private String name;
    
    private trianglePoint a;
    private trianglePoint b;
    private trianglePoint c;
    
    public triangle()
    {
        a = new trianglePoint();
        b = new trianglePoint();
        c = new trianglePoint();
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
        
    public String getImagePath() {
        switch (this.name) {
            case "&Eacute;quilat&eacute;ral":
                return "equilateral.png";
            case "Triangle rectangle":
                return "rectangle.png";
            case "Isoc&egrave;le obtus":
                return "isoscelesObtuse.png";
            case "Isoc&egrave;le aigu":
                return "isoscelesAcute.png";
            case "Scal&egrave;ne obtus":
                return "scaleneObtuse.png";
            case "Scal&egrave;ne aigu":
                return "scaleneAcute.png";
            default:
                return "error.png";
        }
    }
    

    public trianglePoint getA() {
        return this.a;
    }
    
    public trianglePoint getB() {
        return this.b;
    }   
    
    public trianglePoint getC() {
        return this.c;
    } 
}
