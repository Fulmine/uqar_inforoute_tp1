<%@page import="java.time.Period"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>

<%
SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                
                // Date de d�but du projet
                Calendar startC = Calendar.getInstance();
                String startDateString = "26/09/2017 16:15";
                Date startDate = formatter.parse(startDateString);
                startC.setTime(startDate);
                
                // Date de rendu du projet
                Calendar endC = Calendar.getInstance();
                String endDateString = "10/11/2017 00:00";
                Date endDate = formatter.parse(endDateString);
                endC.setTime(endDate);
                
                // Date now
                Calendar c = Calendar.getInstance();
                Date now = new Date();
                c.setTime(now);    
%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Cover Template for Bootstrap</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/custom.css" rel="stylesheet">
  </head>

  <body>

    <div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <div class="header clearfix">
            <div class="inner">
              <nav class="nav nav-header">
                <a class="nav-link" href="index.html">Accueil</a>
                <a class="nav-link active" href="apropos.jsp">� propos</a>
              </nav>
            </div>
          </div>

          <div class="inner cover">
            <img alt="logo" src="assets/img/logo.png"/>
            <h1 class="cover-heading">� propos</h1>
            <p class="lead">Projet cr�� par Jimmy King et Elsa Souyeaux.</p>
            <p class="lead">Il reste

            <%
                long secs = (endDate.getTime() - now.getTime()) / 1000;
                long hours = secs / 3600;
                if (hours < 0)
                    hours = 0;
                out.print(hours);
            %>
            heures avant le rendu du TP<p>
            
            <p class="lead">
            <%
                long secs2 = (now.getTime() - endDate.getTime()) / 1000;
                long hours2 = secs2 / 3600;
                if (hours2 < 0)
                    hours2 = 0;
                out.print(hours2);
            %>
             heures sont pass� depuis le rendu du TP</p>

          </div>

        </div>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery-3.2.1.min.js" />
    <script src="assets/js/bootstrap.min.js"></script>
  </body>
</html>
